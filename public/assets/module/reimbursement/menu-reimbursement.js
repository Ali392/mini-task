const modalCreateReimbur = (data) => {
   return `
   <div class="modal fade" id="modal-reimbur" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header text-bg-primary border-0">
               <h5 class="modal-title font-weight-normal">${data.title}</h5>
               <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
            </div>

            <form action="" method="" id="form-reimbur">
            <div class="modal-body">
               <div class="mb-2">
                  <label for="nama" class="form-label">Tanggal</label>
                  <input class="form-control form-reimbur" name="tanggal" type="date" value="" />
                  <div class="invalid-feedback"></div>
               </div>
               <div class="mb-2">
                  <label for="nama" class="form-label">Nama</label>
                  <input type="text" class="form-control form-reimbur" id="name" name="name" placeholder="" value="">
                  <div class="invalid-feedback"></div>
               </div>
               <div class="mb-2">
                  <label for="nama" class="form-label">Deskripsi</label>
                  <textarea class="form-control form-reimbur" id="description" name="description" type="textarea" placeholder="" rows="3"/></textarea>
                  <div class="invalid-feedback"></div>
               </div>
               <div class="mb-2">
                  <label for="nama" class="form-label">Lampiran</label>
                  <input type="file" class="form-control form-reimbur" id="file" name="file" accept="image/png, image/jpeg, application/pdf" data-max-file-size="500kb">
                  <div class="invalid-feedback"></div>
               </div>
            </div>
            <div class="modal-footer border-0">
               <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Cancel</button>
               <button class="btn btn-primary" type="submit" id="btn-submit-reimbur">Submit</button>
            </div>
            </form>
            
         </div>
      </div>
   </div>
   `
}

const modalConfirm = (title) => {
    return `
   <div class="modal fade" id="modal-confirm" tabindex="-1">
      <div class="modal-dialog w-25">
         <div class="modal-content overflow-hidden">
            <div class="modal-body text-center">
            <h5>Yakin untuk ${title} pengajuan ini?</h6>
            <p class="fs-6 mb-0">Anda akan merubah status pengajuan menjadi ${title}</p>
            </div>
            <div class="row justify-content-center align-items-center p-1 border-top">
            <div class="col-6 border-end">
               <button type="button" class="text-capitalize btn btn-sm text-bold btn-${title == "approve" ? 'success' : 'danger'} w-100 fs-6" id="btn-${title}">${title}</button>
            </div>
            <div class="col-6">
               <button type="button" class="btn btn-sm text-secondary bg-transparent w-100 fs-6" data-bs-dismiss="modal">Batal</button>
            </div>
            </div>
         </div>
      </div>
   </div>
   `
}

export { modalCreateReimbur, modalConfirm }