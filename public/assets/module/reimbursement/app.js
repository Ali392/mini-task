import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalConfirm, modalCreateReimbur } from "./menu-reimbursement.js"

let tableReimbursement, isInsert, content
content = document.querySelector('.content')

tableReimbursement = $('#datatable-reimbursement').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/reimbursement",
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id', orderable: false, searchable: false},
            {data: 'tanggal', name: 'tanggal'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'lampiran', name: 'lampiran'},
            {data: 'status_id', name: 'status_id'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {
            //btn approve surat
            const btnAcc = document.querySelectorAll('.acc-data')
            btnAcc.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                const reimburId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalConfirm("approve"))

                const modalAcc = document.querySelector("#modal-confirm")
                const modalApprove = new bootstrap.Modal(modalAcc)

                modalApprove.show()

                actionPengajuan(reimburId, modalApprove, "approve")

                modalAcc.addEventListener('hidden.bs.modal', () => {
                    modalAcc.remove()
                    this.disabled = false
                })
            }))

            //btn reject surat
            const btnReject = document.querySelectorAll('.reject-data')
            btnReject.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                const reimburId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalConfirm("tolak"))

                const modalRejectPengajuan = document.querySelector("#modal-confirm")
                const modalReject = new bootstrap.Modal(modalRejectPengajuan)

                modalReject.show()

                actionPengajuan(reimburId, modalReject, "tolak")

                modalRejectPengajuan.addEventListener('hidden.bs.modal', () => {
                    modalRejectPengajuan.remove()
                    this.disabled = false
                })
            }))
        }
    })

// create reimbur
const btnCreate = document.querySelector('#add-reimbur')
if(btnCreate != null) {
    btnCreate.addEventListener('click', async function (e) {
        e.preventDefault()
        btnCreate.disabled = true
        isInsert = true
    
        const data = {
            title: 'Pengajuan Reimbur',
        }
    
        try {
            content.insertAdjacentHTML('afterend', modalCreateReimbur(data))
            const modalAdd = document.getElementById('modal-reimbur')
            const modal = new bootstrap.Modal(modalAdd)
            modal.show()
    
            submit(isInsert, modal)
            modalAdd.addEventListener('hidden.bs.modal', () => {
               modalAdd.remove()
               btnCreate.disabled = false
            })
        } catch (error) {
            alert('error', 'Gagal', error.message)
        }
    })
}

// submit
const submit = (isInsert, modal) => {
    const formReimbur = document.querySelector('#form-reimbur')
    formReimbur.addEventListener('submit', async function (e) {
        e.preventDefault()

        const formData = new FormData(this)

        let url
        
        if (isInsert) {
            url = '/reimbursement/store'
        } else {
            url = `/reimbursement/update/`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableReimbursement)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-reimbur', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}

// action acc or reject
const actionPengajuan = (id, modalAction, action) => {
    const btnAction = document.querySelector(`#btn-${action}`)
    btnAction.addEventListener("click", async () => {
        try {
            const formData = new FormData()
            formData.append("_method", "PUT")
            formData.append("action", action)

            const result = await requestData(`/reimbursement/${id}/action`, "POST", formData)

            alert("success", "Berhasil", result.message)
            reloadTable(tableReimbursement)
        } catch (error) {
            alert("error", "Gagal", error.message)
        }

        modalAction.hide()
    })
}