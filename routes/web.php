<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ReimbursementController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::name('user.')->group(function () {
    Route::get('/user', [UserController::class, 'index'])->name('index');
    Route::post('/user/store', [UserController::class, 'store'])->name('store');
    Route::get('/user/{id}/edit', [UserController::class, 'edit'])->name('edit');
    Route::patch('/user/update/{id}', [UserController::class, 'update'])->name('update');
    Route::delete('/user/delete/{id}', [UserController::class, 'destroy'])->name('delete');
});

Route::name('reimbursement.')->group(function () {
    Route::get('/reimbursement', [ReimbursementController::class, 'index'])->name('index');
    Route::post('/reimbursement/store', [ReimbursementController::class, 'store'])->name('store');
    Route::put('/reimbursement/{reimbursement}/action', [ReimbursementController::class, 'changeStatus']);
    Route::get('/reimbursement/download/{reimburs}', [ReimbursementController::class, 'downloadFile'])->name('download');
});
