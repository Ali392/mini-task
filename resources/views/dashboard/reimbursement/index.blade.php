@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>Reimbursement</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">Pengajuan Reimbursement</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
			@if(auth::user()->jabatan == 3)
			<div class="row">
					<div class="col d-flex justify-content-end p-2">
							<a class="btn btn-primary" href="#" id="add-reimbur">
									<i class="bi bi-plus-square me-1"></i>
									Ajukan Reimbursement
							</a>
					</div>
			</div>
			@endif

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Pengajuan Reimbursement</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-reimbursement">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tanggal</th>
										<th scope="col">Nama</th>
										<th scope="col">Deskripsi</th>
										<th scope="col">Lampiran</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/reimbursement/app.js') }}" type="module"></script>
@endsection