<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*home*') ? '' : 'collapsed' }}" href="{{ route('home') }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading">Menu</li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*user*') ? '' : 'collapsed' }}" href="{{ route('user.index') }}">
          <i class="bi bi-people"></i>
          <span>Akun Pengguna</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link {{ Request::is('*reimbursement*') ? '' : 'collapsed' }}" href="{{ route('reimbursement.index') }}">
          <i class="bi bi-card-list"></i>
          <span>Reimbursement</span>
        </a>
      </li>

    </ul>

  </aside>