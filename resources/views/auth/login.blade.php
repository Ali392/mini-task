@extends('layouts.auth-app')
@section('content')
	<section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
		<div class="container">
				<div class="row justify-content-center">
						<div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

								<div class="d-flex justify-content-center py-4">
									<a href="index.html" class="logo d-flex align-items-center w-auto">
										<img src="{{ asset('assets/img/logo.png') }}" alt="logo">
										<span class="d-none d-lg-block">NiceAdmin</span>
									</a>
								</div><!-- End Logo -->

								<div class="card mb-3">

										<div class="card-body">

												<div class="pt-4 pb-2">
														<h5 class="card-title text-center pb-0 fs-4">Login</h5>
														<p class="text-center small">Masukkan username & password untuk login</p>
												</div>

												@if ($errors->has('nip'))
												<div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding:.25rem;">
														{{ $errors->first('nip') }}
														<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="padding:.5rem;"></button>
												</div>
												@endif

												<form class="row g-3 needs-validation" novalidate method="POST" action="{{ route('login') }}">
														@csrf

														<div class="col-12">
																<label for="yourUsername" class="form-label">NIP :</label>
																<div class="input-group has-validation">
																		<!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
																		<input type="text" name="nip" value="{{ old('nip') }}"
																				class="form-control" id="nip" required>

																		@error('nip')
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																				</span>
																		@enderror
																</div>
														</div>

														<div class="col-12">
																<label for="yourPassword" class="form-label">Password :</label>
																<div class="input-group has-validation">
																		<input type="password" name="password"
																				class="form-control @error('password') is-invalid @enderror" id="password"
																				required autocomplete="current-password">
																		<div class="position-absolute" style="top: 50%; left: 95%; transform: translate(-50%, -50%); cursor: pointer;z-index:1000;">
																				<span class="toggle-eye text-dark">
																				<i class="bi bi-eye-fill"></i>
																				</span>
																		</div>

																		@error('password')
																				<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																				</span>
																		@enderror
																</div>
														</div>

														<hr class="mt-4 mb-2">

														<div class="col-12">
																<div class="form-check">
																		<input class="form-check-input" type="checkbox" name="remember" value="true"
																				id="rememberMe">
																		<label class="form-check-label" for="rememberMe">Remember me</label>
																</div>
														</div>
														<div class="col-12 mb-2">
																<button class="btn btn-primary w-100" type="submit">Login</button>
														</div>
														<div class="col-12">
															<p class="small mb-0">Don't have account? <a href="{{ route('register') }}">Create an account</a></p>
														</div>
												</form>

										</div>
								</div>

						</div>
				</div>
		</div>

</section>
@endsection
