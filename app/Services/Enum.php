<?php

namespace App\Services;

use BenSampo\Enum\Enum as Enums;

class Enum extends Enums
{
    /**
     * Get the enum as descriptions key value.
     *
     * [mixed $value => string description]
     *
     * @return array
     */
    public static function asComment()
    {
        $arr = [];

        foreach (self::toSelectArray() as $key => $value) {
            $arr[] = "$key = $value";
        }

        return implode(", ", $arr);
    }

    /**
     * Get the enum as an array formatted for a select.
     *
     * [mixed $value => string description]
     *
     * @param  array  $key
     * @return array
     */
    public static function toSelectArray(array $enums = []): array
    {
        $selectArray = [];
        $array = count($enums) ? $enums : static::asArray();

        foreach ($array as $value) {
            $selectArray[$value] = static::getDescription($value);
        }

        return $selectArray;
    }
}