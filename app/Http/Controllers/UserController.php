<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = User::all();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($user) {
                $state = [
                    'disable' => Auth::user()->jabatan == 1 ? '' : 'disabled',
                ];

                $button = "
                    <div class='d-flex'>
                        <button class='btn btn-info btn-sm show-data' data-id='$user->id' type='button' $state[disable]><i class='bi bi-eye'></i></button>
                        <button class='btn btn-warning btn-sm mx-1 edit-data' data-id='$user->id' type='button' $state[disable]><i class='bi bi-pencil'></i></button>
                        <button class='btn btn-danger btn-sm delete-data' data-id='$user->id' type='button' $state[disable]><i class='bi bi-trash'></i></button>
                    </div>
                    ";

                return $button;
                
              })
              ->editColumn('jabatan', function ($user) {
                if ($user->jabatan == 1) {
                    return '<span class="badge bg-primary">DIREKTUR</span>';
                } elseif ($user->jabatan == 2) {
                    return '<span class="badge bg-warning">FINANCE</span>';
                } else {
                    return '<span class="badge bg-secondary">STAFF</span>';
                }
              })
              ->rawColumns(['action', 'jabatan'])
              ->make('true');
        }

        return view('dashboard.user.index');
    }

    public function store(Request $request)
    {
        $rules = [
            'nip' => 'required|unique:users,nip',
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'jabatan' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
            'email' => ':attribute harus valid',
            'same' => ':attribute harus sama dengan password',
            'unique' => ':attribute sudah dipakai',
            'min' => ':attribute harus lebih dari :min karakter',
        ];

        $aliases = [
            'nip' => 'NIP',
            'name' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'password_confirmation' => 'Konfirmasi Password',
        ];

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            $validated['password'] = Hash::make($request->password);

            $user = User::create($validated);
            $user->save();

            return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal ditambahkan'];
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $user = User::find($id);
                $jabatan = [];
                $jabatan[1] = 'DIREKTUR';
                $jabatan[2] = 'FINANCE';
                $jabatan[3] = 'STAFF';

                return ['status' => 200, 'user' => $user, 'jabatan' => $jabatan];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();

        $rules = [
            'nip' => 'required|unique:users,nip,' . $user->id,
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'jabatan' => 'required',
        ];

        $messages = [
            'required' => ':attribute harus diisi',
            'email' => ':attribute harus valid',
            'same' => ':attribute harus sama dengan password',
            'unique' => ':attribute sudah dipakai',
            'min' => ':attribute harus lebih dari :min karakter',
        ];

        $aliases = [
            'nip' => 'NIP',
            'name' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'password_confirmation' => 'Konfirmasi Password',
        ];

        if ($request->password) {
            $rules['password'] = ['min:6'];
            $rules['password_confirmation'] = ['same:password'];
        }

        $validator = Validator::make($request->all(), $rules, $messages, $aliases);

        if ($validator->fails()) {
            return ['status' => 422, 'validation' => $validator->getMessageBag()];
        }

        try {
            $validated = $validator->validated();
            if($request->password) {
                $validated['password'] = Hash::make($request->password);
            }

            $user->update($validated);

            return ['status' => 200, 'message' => 'Data berhasil diubah'];
        } catch (\Throwable $th) {
            return ['status' => 500, 'message' => 'Data gagal diubah'];
        }
    }

    public function destroy(User $id)
    {
        if (request()->ajax()) {
            // check if user delete her self
            if(Auth::user() == $id) {
                return ['status' => 500, 'message' => 'User sedang aktif!'];
            }

            try {
              $id->delete();
              return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
              return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }
}
