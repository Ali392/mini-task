<?php

namespace App\Http\Controllers;

use App\Enums\PengajuanStatus;
use App\Models\Reimbursement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Yajra\DataTables\DataTables;

class ReimbursementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->ajax()) {

            // list reimbursement based user
            if(Auth::user()->jabatan == 1 || Auth::user()->jabatan == 3) {
                $data = Reimbursement::all();
            } else {
                $data = Reimbursement::whereIn('status_id', [PengajuanStatus::DISETUJUI, PengajuanStatus::DISETUJUIFINANCE, PengajuanStatus::DITOLAKFINANCE])->get();
            }
                return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($reimburs) {
                    if(Auth::user()->jabatan != 3) {
                        $button = "
                            <div class='d-flex'>
                                <button class='btn btn-success btn-sm me-1 acc-data' data-id='$reimburs->id' type='button'><i class='bi bi-check2-square me-1'></i>Setujui</button>
                                <button class='btn btn-danger btn-sm reject-data' data-id='$reimburs->id' type='button'><i class='bi bi-x-square me-1'></i>Tolak</button>
                            </div>
                            ";
                    } else {
                        $button = "
                            <div class='d-flex'>
                                <button class='btn btn-secondary btn-sm' type='button' disabled><i class='bi bi-dash-square'></i></button>
                            </div>
                            ";
                    }


                    return $button;
                
                })
                 ->editColumn('tanggal', function ($reimburs) {
                    $date = Carbon::parse($reimburs->tanggal)->isoFormat('D MMMM Y');
                    return $date;
                })
                ->editColumn('status_id', function ($reimburs) {
                    return '<span class="' . PengajuanStatus::asBadge($reimburs->status_id) . '">' . PengajuanStatus::getDescription($reimburs->status_id) . '</span>';
                })
                ->editColumn('lampiran', function ($reimburs) {
                    $value = $reimburs->getFirstMedia('reimburs')->file_name ?? '(kosong)';

                    if($value != '(kosong)') {
                        $link = '<a href="/reimbursement/download/' . $reimburs->getFirstMedia('reimburs')->id . '">' . $value .'</a>';
                    } else {
                        $link = '(kosong)';
                    }

                    return $link;
                })
                ->rawColumns(['action', 'status_id', 'lampiran'])
                ->make('true');
        }

        return view('dashboard.reimbursement.index');
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {

            $rules = [
                'tanggal' => 'required',
                'name' => 'required',
                'description' => 'required',
            ];

            $messages = ['required' => ':attribute harus diisi',];

            $aliases = [
                'tanggal' => 'Tanggal',
                'name' => 'Nama Pengajuan',
                'description' => 'Deskripsi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages, $aliases);

            if ($validator->fails()) {
                return ['status' => 422, 'validation' => $validator->getMessageBag()];
            }

            try {
                $validated = $validator->validated();
                $validated['user_id'] = Auth::user()->id;

                $reimburs = Reimbursement::create($validated);

                // insert file if exist
                if (isset($request->file)) {
                    $reimburs->addMedia($request->file('file'))->toMediaCollection('reimburs');
                }

                $reimburs->save();

                return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal ditambahkan'];
            }
        }
    }

    public function changeStatus(Request $request, Reimbursement $reimbursement)
    {
        if ($request->ajax()) {
            try {
                $action = $request->action;

                if ($action == 'approve') {
                    if(Auth::user()->jabatan == 1) {
                        $reimbursement->update(['status_id' => PengajuanStatus::DISETUJUI]);
                    } else {
                        $reimbursement->update(['status_id' => PengajuanStatus::DISETUJUIFINANCE]);
                    }
                }

                if ($action == 'tolak') {
                    if(Auth::user()->jabatan == 1) {
                        $reimbursement->update(['status_id' => PengajuanStatus::DITOLAK]);
                    } else {
                        $reimbursement->update(['status_id' => PengajuanStatus::DITOLAKFINANCE]);
                    }
                }

                return ['status' => 200, 'message' => "Pengajuan berhasil di $action"];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Pengajuan gagal diubah'];
            }
        }
    }

    public function downloadFile(Media $reimburs)
    {
        return response()->download($reimburs->getPath(), $reimburs->file_name);
    }
}
