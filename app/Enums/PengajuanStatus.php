<?php declare(strict_types=1);

namespace App\Enums;

use App\Services\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class PengajuanStatus extends Enum
{
    const PENDING          = 1;
    const DISETUJUI        = 2;
    const DITOLAK          = 3;
    const DISETUJUIFINANCE = 4;
    const DITOLAKFINANCE   = 5;

    public static function asBadge($value)
    {
        switch ($value) {
            case (self::PENDING):
                return 'badge bg-warning';
            case (self::DISETUJUI):
                return 'badge bg-success';
            case (self::DITOLAK):
                return 'badge bg-danger';
            case (self::DISETUJUIFINANCE):
                return 'badge bg-success';
            case (self::DITOLAKFINANCE):
                return 'badge bg-danger';
        }
    }

    public static function getDescription($value): string
    {
        if ($value === self::PENDING) {
            return 'Pending';
        } elseif ($value === self::DISETUJUI) {
            return 'Disetujui Direktur';
        } elseif ($value === self::DITOLAK) {
            return 'Ditolak Direktur';
        } elseif ($value === self::DISETUJUIFINANCE) {
            return 'Disetujui Finance';
        } elseif ($value === self::DITOLAKFINANCE) {
            return 'Ditolak Finance';
        }

        return parent::getDescription($value);
    }
}
