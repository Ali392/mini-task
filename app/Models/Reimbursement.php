<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Reimbursement extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = ['id'];

    protected $appends = [
        'file_name', 'file_url', 'file_size'
    ];

    public function getFileNameAttribute()
    {
        return $this->getFirstMedia('reimburs')->file_name ?? "Kosong";
    }

    public function getFileUrlAttribute()
    {
        return $this->getFirstMediaUrl('reimburs') ?? "";
    }

    public function getFileSizeAttribute()
    {
        return $this->getFirstMedia('reimburs')->size ?? "Kosong";
    }

}
