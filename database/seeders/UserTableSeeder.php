<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::insert([
            [
                'nip' => '1234',
                'name' => 'DONI',
                'email' => 'doni@mail.com',
                'email_verified_at' => Carbon::now(),
                'jabatan' => 1,
                'password' => Hash::make('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'nip' => '1235',
                'name' => 'DONO',
                'email' => 'dono@mail.com',
                'email_verified_at' => Carbon::now(),
                'jabatan' => 2,
                'password' => Hash::make('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'nip' => '1236',
                'name' => 'DONA',
                'email' => 'dona@mail.com',
                'email_verified_at' => Carbon::now(),
                'jabatan' => 3,
                'password' => Hash::make('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
