<?php

use App\Enums\PengajuanStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reimbursements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('tanggal')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->unsignedSmallInteger('status_id')->default(PengajuanStatus::PENDING)->comment(PengajuanStatus::asComment());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reimbursements');
    }
};
